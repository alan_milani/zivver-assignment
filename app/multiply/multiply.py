#!/usr/bin/env python3
from flask import Flask, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/', methods=['POST'])
@cross_origin()
def index():
    try:
        op1 = int(request.json['op1'])
        op2 = int(request.json['op2'])
    except Exception as e:
        return "Exception: {0}".format(e)
    return "Result: {0}".format(str(op1 * op2))

if __name__ == '__main__':
    app.run(host="0.0.0.0",port="8000")
