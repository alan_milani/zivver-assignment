# zivver-assignment

# Overview

This repository contains the code to deploy the infrastructure to run an Application on AWS EKS and the Application itself

The Application is stored on "app" folder split in 3 containers:
1. A REST API that will sum 2 values passed to it (op1 and op2) through POST method
2. A REST API that will multiply 2 values passed to it (op1 and op2) through POST method
3. An nginx containing a simple HTML with some Javascript code that will call the REST APIs when is pressed the "Run sum" or the "Run multiply" button

The Terraform code is stored in the "terraform" folder

The Manifests to deploy the Application on Kubernetes is stored in the "manifests" folder

# Infra deployment

The infrastructure can be deployed using the procedure below:

## Pre-requisites

To be able to deploy successfully the infrastructure it's necessary that:
1. The AWS credentials with permissions to deploy new resources on AWS is present in the machine that will run the Terraform code
2. The DNS zone must exist already and be public
3. The kubectl and helm commands should be installed already

## Infra creation using Terraform

To create the Infrastructure using Terraform go to the folder terraform/infra

Update the terraform.tfvars values accordingly to reflect the intended values

Update the terraform.tf to store the state in the proper S3 bucket

Run: 
1. cd terraform/infra # It considers you were in the repository root folder
2. terraform init # It will initialize the Terraform environment
3. terraform plan # Check the changes Terraform will apply to the infrastructure
4. terraform apply # Type "yes" and press <ENTER> if you agree with the changes

It takes some time to create the EKS cluster

## Installation of the Traefik Ingress Controller

After the cluster is created we need to install the Traefik Ingress Controller using Helm

Procedure:
1. aws eks update-kubeconfig --name my-eks # Change the "my-eks" to the name of the EKS Cluster you are connecting to. This command updates the kubectl config
2. helm repo add traefik https://traefik.github.io/charts # It adds the Traefik repository to Helm
3. helm repo update # It updates the repository index
4. helm install traefik traefik/traefik # It installs Traefik Ingress Controller to the cluster

## DNS creation

The DNS entries are based on the Load Balancer created during the Traefik Ingress Controller installation. So we have to create the DNS entries only after install Traefik Ingress Controller

Update the terraform.tfvars values accordingly to reflect the intended values

Update the terraform.tf to store the state in the proper S3 bucket

To create the DNS entries using Terraform use the procedure below:
1. cd terraform/dns # It considers you were in the repository root folder 
2. terraform init # It will initialize the Terraform environment
3. terraform plan # Check the changes Terraform will apply to the infrastructure
4. terraform apply # Type "yes" and press <ENTER> if you agree with the changes

# Application deployment

The .gitlab-ci.yml was configured to build and deploy all the containers automatically whenever a change is detected in the content of the repository

To start the deployment of the Application it's necessary to make any change in the Application and merge to the repository

# Improvements

There are some improvements that could be implemented:
1. Install cert-manager + Let's Encrypt to have HTTPS enabled
2. Automate the infrastructure deployment using a pipeline
3. Change the Application to run on server side, so the containers wouldn't need to be accessible publicly
4. Optimize the CI/CD to build and deploy only updated containers
