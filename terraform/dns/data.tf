data "kubernetes_service" "traefik" {
  metadata {
    name = "traefik"
    namespace = "traefik"
  }
}

data "aws_route53_zone" "dns-zone-name" {
  name         = var.dns_zone_name
  private_zone = false
}
