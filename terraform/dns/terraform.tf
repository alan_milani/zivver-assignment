provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "zivver-assignment-terraform"
    key    = "zivver-assignment-terraform-dns"
    region = "eu-central-1"
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}
