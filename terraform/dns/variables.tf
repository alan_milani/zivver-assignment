# DNS

variable "dns_cnames" {
  type        = list(string)
  description = "A list with DNS CNAMEs to be pointed to the Traefik Ingress Controller Load Balancer"
}

variable "dns_zone_name" {
  type        = string
  description = "The name of the DNS zone name the CNAME entries will be created"
}
