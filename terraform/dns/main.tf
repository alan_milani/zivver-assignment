resource "aws_route53_record" "testcname" {
  for_each = toset(var.dns_cnames)
  zone_id = data.aws_route53_zone.dns-zone-name.zone_id
  name    = "${each.value}.${data.aws_route53_zone.dns-zone-name.name}"
  type    = "CNAME"
  ttl     = "300"
  records = [data.kubernetes_service.traefik.status.0.load_balancer.0.ingress.0.hostname]
}
