# VPC
vpc_name            = "main"
vpc_cidr            = "10.0.0.0/16"
vpc_azs             = ["eu-central-1a", "eu-central-1b"]
vpc_private_subnets = ["10.0.0.0/19", "10.0.32.0/19"]
vpc_public_subnets  = ["10.0.64.0/19", "10.0.96.0/19"]

# ECR
ecr_names = ["sum", "multiply", "nginx"]

# EKS
eks_name            = "my-eks"
eks_cluster_version = "1.25"
eks_node_disk_size  = 50
eks_instance_types  = ["t3.small"]
