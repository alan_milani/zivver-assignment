provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "zivver-assignment-terraform"
    key    = "zivver-assignment-terraform-state"
    region = "eu-central-1"
  }
}
