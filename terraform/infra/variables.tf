# VPC

variable "vpc_name" {
  type        = string
  description = "The VPC module name"
}

variable "vpc_cidr" {
  type        = string
  description = "The VPC CIDR"
}

variable "vpc_azs" {
  type        = list(string)
  description = "A list with all the Availability Zones of the VPC"
}

variable "vpc_private_subnets" {
  type        = list(string)
  description = "A list with all the Private Subnets"
}

variable "vpc_public_subnets" {
  type        = list(string)
  description = "A list with all the Public Subnets"
}

# EKS

variable "eks_name" {
  type        = string
  description = "The EKS name"
}

variable "eks_cluster_version" {
  type        = string
  description = "The EKS Cluster version"
}

variable "eks_node_disk_size" {
  type        = string
  description = "The disk size for the Managed Nodes on EKS"
}

variable "eks_instance_types" {
  type        = list(string)
  description = "A list with all the allowed instance types for the managed nodes on EKS"
}

# ECR

variable "ecr_names" {
  type = list(string)
}
